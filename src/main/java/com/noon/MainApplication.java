package com.noon;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.sql.*;
import java.util.Map;

public class MainApplication {
    public static void main(String[] args) {
        System.out.println("args: " + args);

        String fileName = args[0];
        String jdbcConnectionString = args[1];
        String userName = args[2];
        String password = args[3];

        File file = new File(fileName);
        MainApplication mainApplication = new MainApplication();
        ObjectMapper objectMapper = new ObjectMapper();
        Connection connection = mainApplication.createConnection(jdbcConnectionString, userName, password);

        try {
            BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

            String st;
            while ((st = bufferedReader.readLine()) != null) {
                Map<String, Object> mainEvent = objectMapper.readValue(st, Map.class);
                Map<String, Integer> userDetailsMap = objectMapper.readValue(mainEvent.get("msg").toString(), Map.class);

//                System.out.println("Guest: " + userDetailsMap.get("guest_user_id"));

                mainApplication.updateUserIdInUserService(connection, userDetailsMap.get("guest_user_id"), userDetailsMap.get("user_id"));
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void updateUserIdInUserService(Connection connection, Integer guestUserId, Integer userId) {
        try {
            System.out.println("Updating user_device_relation for guest_user_id, user_id - " + guestUserId + ", " + userId);

            String updateQuery = "UPDATE user_device_relation SET converted_user_id=? WHERE user_id=? and user_type='GUEST' and (action='SIGNUP' or action is null) and converted=true and converted_user_id is null";
            PreparedStatement preparedStatement = connection.prepareStatement(updateQuery);
            preparedStatement.setInt(1, userId);
            preparedStatement.setInt(2, guestUserId);

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("Update failed for guest_user_id, user_id - " + guestUserId + ", " + userId);
        }
    }

    private Connection createConnection(String jdbcConnectionString, String userName, String password) {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            connection = DriverManager.getConnection(
                    jdbcConnectionString, userName, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return connection;
    }
}
